package com.shubhankar.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shubhankar.dao.CustomerDao;
import com.shubhankar.entities.Customer;

@Transactional
@Service
public class CustomerServicesImpl implements CustomerServices {
	
	@Autowired
	private CustomerDao custDao;
	
	@Override
	public Customer findByEmail(String email) {
		Customer cust = custDao.findByEmail(email);
		return cust;
	}
	
	@Override
	public Customer authenticate(String email, String password) {
		Customer cust = findByEmail(email);
		if(cust != null && cust.getPassword().equals(password)) return cust;
		return null;
	}
}
