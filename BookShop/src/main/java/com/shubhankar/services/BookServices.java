package com.shubhankar.services;

import java.util.List;

import com.shubhankar.entities.Book;

public interface BookServices {
	String[] findDistinctSubject();
	List<Book> findBookBySubject(String subject);
	Book findBookById(int id);
}
