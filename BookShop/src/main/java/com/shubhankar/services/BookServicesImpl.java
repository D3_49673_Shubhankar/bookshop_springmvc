package com.shubhankar.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shubhankar.dao.BookDao;
import com.shubhankar.entities.Book;

@Transactional
@Service
public class BookServicesImpl implements BookServices {
	
	@Autowired
	private BookDao bookDao;
	
	@Override
	public String[] findDistinctSubject() {
		String[] subject = bookDao.findDistinctSubject();
		return subject;
	}

	@Override
	public List<Book> findBookBySubject(String subject) {
		List<Book> books = bookDao.findBookBySubject(subject);
		return books;
	}
	 @Override
	public Book findBookById(int id ) {
		Book book = bookDao.findBookById(id);
		 return book;
	}
}
