package com.shubhankar.services;

import com.shubhankar.entities.Customer;

public interface CustomerServices {
	Customer findByEmail(String email);
	
	Customer authenticate(String email, String password);
}
