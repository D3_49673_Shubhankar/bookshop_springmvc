package com.shubhankar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shubhankar.entities.Book;
import com.shubhankar.services.BookServices;

@SpringBootApplication
public class BookShopApplication implements CommandLineRunner{
	
	
	@Autowired
	private BookServices bookService;
	
	public static void main(String[] args) {
		SpringApplication.run(BookShopApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
//				String[] subject = bookService.findDistinctSubject();
//				for (String string : subject) {
//					System.out.println(string);
//				}
//				List<Book> books = bookService.findBookBySubject("OS");
//				books.forEach(System.out::println);
		Book book =bookService.findBookById(11);
		System.out.println(book);
	}
}
