package com.shubhankar.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shubhankar.entities.Book;

public interface BookDao extends JpaRepository<Book, Integer>{
	
	@Query("select distinct b.subject from Book b")
	String[] findDistinctSubject();
	List<Book> findBookBySubject(String subject);
	Book findBookById(int id);
}
