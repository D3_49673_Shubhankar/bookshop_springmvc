package com.shubhankar.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shubhankar.entities.Customer;

public interface CustomerDao extends JpaRepository<Customer, Integer> {
	Customer findByEmail(String email);
}
