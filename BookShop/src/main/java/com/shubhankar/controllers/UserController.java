package com.shubhankar.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.shubhankar.entities.Book;
import com.shubhankar.entities.Customer;
import com.shubhankar.models.Credentials;
import com.shubhankar.services.BookServices;
import com.shubhankar.services.CustomerServices;

@Controller
public class UserController {
	
	@Autowired	
	private CustomerServices custService;
	
	@Autowired
	private BookServices bookService;
	
	@RequestMapping("/autthenticate")
	public String authenticate(Credentials cred, HttpSession session, Model model) {
		Customer cust = custService.authenticate(cred.getEmail(), cred.getPassword());
		String[] subject = bookService.findDistinctSubject();
		if(cust != null) {
			session.setAttribute("cust", cust);
			model.addAttribute("customer", cust);
			model.addAttribute("subjects", subject);
			return "subject";
		}
		return null;
	}
	
	@RequestMapping("/books")
	public String getBooks(@RequestParam(name = "subject") String subject, Model model, HttpSession session) {
		List<Book> books =bookService.findBookBySubject(subject);
		model.addAttribute("books", books);
		model.addAttribute("customer", session.getAttribute("cust"));
		return "books";
	}
	
	@RequestMapping("/addtocart")
	public String addCart(@RequestParam(name = "bookid") String[] bookids, HttpSession session, Model model) {
		List<Book> cart = new ArrayList<Book>();
		for (String id : bookids) {
			 int bookId = Integer.parseInt(id);
			 Book  book= bookService.findBookById(bookId);
			 cart.add(book);
		}
		String[] subject = bookService.findDistinctSubject();
		session.setAttribute("cart", cart);
		model.addAttribute("customer", session.getAttribute("cust"));
		model.addAttribute("subjects", subject);
		return "subject";
	}
	
	@RequestMapping("/showcart")
	public String showcart(HttpSession session, Model model) {
		List<Book> cart = (List<Book>) session.getAttribute("cart");
		model.addAttribute("customer", session.getAttribute("cust"));
		model.addAttribute("cart", cart);
		return "showcart";
	}
	
	@RequestMapping("/logout")
	public String logout() {
		return "logout";
	}
}
