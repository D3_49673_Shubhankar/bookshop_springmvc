<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Subject</title>
</head>
<body>
	<h3>Hello, ${customer.name}</h3>
	</br>
	<hr>
	<form method="get" action="books">
		<c:forEach var="sub" items="${subjects}">
			<input type="radio" name = "subject" value="${sub }"/> ${sub} </br>
		</c:forEach>
		<input type="submit" value="Show Books" /> <a href="showcart" type="button">ShowCart</a>
	</form>
</body>
</html>