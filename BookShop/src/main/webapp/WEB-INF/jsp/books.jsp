<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
</head>
<body>
		<h3>Hello, ${customer.name}</h3>
		</br>
		<hr>
		<form method="get" action="addtocart">
			<c:forEach var="b" items="${books}">
				<input type="checkbox" name = "bookid" value="${b.id}"/> ${b.name} </br>
			</c:forEach>
		<input type="submit" value="Add to cart" />
	</form>
</body>
</html>