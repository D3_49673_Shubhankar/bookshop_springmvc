<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
</head>
<body>
	<h3>Hello, ${customer.name}</h3>
		</br>
		<hr>
		<c:set value="0.0" var="total"/>
		<table border="1">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Author</td>
				<td>Subject</td>
				<td>Price</td>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="book" items="${cart}">
			<tr>
				<td>${book.id}</td>
				<td>${book.name}</td>
				<td>${book.author}</td>
				<td>${book.subject}</td>
				<td>${book.price}</td>
			</tr>
			<c:set var="total" value="${total + book.price }"/>
		</c:forEach>
		</tbody>
	</table>
	Total Price: Rs. <fmt:formatNumber value="${total}" type="number" minFractionDigits="2" maxFractionDigits="2" />/-
		<br/><br/>
		<a href="logout">Sign Out</a>
</body>
</html>